package com.accolite.HospitalAppointmentSystem.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.accolite.HospitalAppointmentSystem.util.Constant;

public class ApacheHttpClientDoctorPut extends Constant {

	public static void main(String[] args) {

		try {

			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPut putRequest = new HttpPut(
					SERVER_URL+API_URL+URL_DCTR+"/{1}");

			StringEntity input = new StringEntity(
					"{"
					//+ "\"id\":103,"
					//+ "\"firstName\":\"Lohit\","
					//+ "\"lastName\":\"kumar\","
					//+ "\"phoneNumber\":\"123456789\","
					+ "\"departmentName\":\"PHD\"}");
			input.setContentType("application/json");
			putRequest.setEntity(input);

			HttpResponse response = httpClient.execute(putRequest);

			if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {

				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();

		}

	}

}