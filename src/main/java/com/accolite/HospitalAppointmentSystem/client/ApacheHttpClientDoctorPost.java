package com.accolite.HospitalAppointmentSystem.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.accolite.HospitalAppointmentSystem.util.Constant;

public class ApacheHttpClientDoctorPost extends Constant {

	public static void main(String[] args) {

		try {

			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(
					SERVER_URL+API_URL+URL_DCTR+URL_ADD);

			StringEntity input = new StringEntity(
					"{\"id\":103,"
					+ "\"firstName\":\"Lohit\","
					+ "\"lastName\":\"kumar\","
					+ "\"phoneNumber\":\"123456789\","
					+ "\"departmentName\":\"BNT\"}");
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);

			if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {

				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();

		}

	}

}