package com.accolite.HospitalAppointmentSystem.util;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_INDX;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_INDX_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_STATUS;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_STATUS_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.INDEX_DEFAULT;
import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_AVAILABLE;
import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_BOOKED;
import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_REST;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_END_TIME;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_START_TIME;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_REST_TIME_IN_MIN;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.bean.TimeSlot;
import com.accolite.HospitalAppointmentSystem.exception.InvalidArgumentException;
import com.accolite.HospitalAppointmentSystem.exception.InvalidIndexException;
import com.accolite.HospitalAppointmentSystem.exception.InvalidStatusException;

public class UtilClass {

	static Logger log = LoggerFactory.getLogger(UtilClass.class);
	
	/**
	 * Method checks if newly added appointment (@param key) is conflicting with existing appointment(@param root)
	 * @param root
	 * @param key
	 * @return true if confilict
	 */
	public static boolean isConflict(Appointment root, Appointment key) {
		
		boolean retVal = false;
		if(root.getAppDate().isEqual(key.getAppDate()))
	    {
	    	LocalTime rootAppEndTime = root.getAppTime().plus(root.getDuration());
    		LocalTime keyAppEndTime =  key.getAppTime().plus(key.getDuration());
    		
	    	if(keyAppEndTime.isAfter(root.getAppTime()) && keyAppEndTime.isBefore(rootAppEndTime)) {
	    		
	    		retVal = true;
	    	}else if(key.getAppTime().isAfter(root.getAppTime()) && key.getAppTime().isBefore(rootAppEndTime)) {
	    		retVal = true;
	    	}
	    }
	
		return retVal;
	}

	public static String[] initStatus(HashMap<Integer, String[]> calender, int doctorId) {
		
		String status[] ;
		if(calender.containsKey(doctorId)) {
		status = calender.get(doctorId);	
		}else {
			status= new String[(WORK_END_TIME-WORK_START_TIME)*60 +1];
			for(int i=0; i<status.length; i++) {
				status[i] = STATUS_AVAILABLE;
			}
		}
		
		return status;
	}

	public static boolean isConflict(Appointment app, String[] status) {
		boolean retVal = false;
		
		int indexFrom = UtilClass.getStatusIndexFromTime(app.getAppTime());
		int indexTo = indexFrom + (int)app.getDuration().toMinutes();
		

		for(int i = indexFrom; i<=indexTo; i++) {
			
			/*if(status==null)
			{
				break;
			}
			*/
			if(!status[i].equals(STATUS_AVAILABLE)) {
				retVal = true;
				log.debug("########## : Min" + i +"is already booked and status is "+status[i]);
				break;
			}
		}
		
		return retVal;
	}
	
	/*public static void main(String[] args) {
		
		Appointment app =new Appointment();
		app.setAppTime(LocalTime.of(9, 10));
		app.setDuration(Duration.ofMillis(600000));
		System.out.println(app.getDuration().toMinutes());
		//bookMinutes(app, new String[481]);
		
		//isConflict();
	}*/

	public static String[] bookMinutes(Appointment app, String[] status) {
		

		int indexFrom = UtilClass.getStatusIndexFromTime(app.getAppTime());
		int indexTo = indexFrom + (int)app.getDuration().toMinutes();
	
		
		System.out.println("########## BOOKING CALENDER ###############");
		System.out.println("indexFrom : "+indexFrom);
		System.out.println("indexTo : "+indexTo);
		for(int i = indexFrom; i<=indexTo+WORK_REST_TIME_IN_MIN; i++) {
			if(i<=indexTo)
			 status[i]= STATUS_BOOKED;
			else
			status[i]= STATUS_REST;
		}
		

	
		return status;
	}

	public static int getStatusIndexFromTime(LocalTime time) {
	
		return ( (time.minusHours(WORK_START_TIME).getHour() * 60 )+ time.getMinute());
		
	}

	public static ArrayList<TimeSlot> builAvailableTimeSlots(Appointment app, String[] status) {
		
		ArrayList<TimeSlot> availableSlots = new ArrayList<>();
		/**
		 * we have status array like below
		 *  __________________________________________________________________________________________________________________________
		 * | A | A | A | A | A | B | B | B | B | B | R | R | A | A | A | B | B |B  | R | R | A | A | A | A | A | A | A | A | A| A | A |
		 * ````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
		 * we have to find all set of A.
		 * slot near to requested slot will be preferred. This preference is maintained through rank variable for each slot
		 *
		 */
		int indexFrom = INDEX_DEFAULT, indexTo=INDEX_DEFAULT;
		for(int i = 0; i<status.length; i++) {
			
			if(!StringUtils.isEmpty(status[i])) {
				
			
			if(status[i].equals(STATUS_AVAILABLE)) {
				
				if(isIndexInitialized(indexFrom)) {
					++indexTo;
				}else {
					
					indexFrom = i;
					indexTo = i;
				}
			}else {
				// status is Either BOOKED or REST
				
				if(isIndexInitialized(indexFrom) && isIndexInitialized(indexTo)) {
					
					if(indexFrom > indexTo) {
						log.error(ERROR_CODE_INVLD_INDX+"->"+ERROR_CODE_INVLD_INDX_MSG);
						throw new InvalidIndexException(ERROR_CODE_INVLD_INDX);
					}
				
				
					//build the rank of time slot
					int rank = buildRankofCurrentAvailableSlot(app, indexFrom);
					// build time slot object
					TimeSlot timeSlot = new TimeSlot(indexFrom, indexTo,rank);
				
					availableSlots.add(timeSlot);
					indexFrom = indexTo =INDEX_DEFAULT;
				}
				
			}
		}else {
			
			log.error(ERROR_CODE_INVLD_STATUS+"->"+ERROR_CODE_INVLD_STATUS_MSG);
			throw new InvalidStatusException(ERROR_CODE_INVLD_STATUS+"->"+ERROR_CODE_INVLD_STATUS_MSG);
			
		}
		
		} // end of for loop
		return availableSlots;
	}


	private static int buildRankofCurrentAvailableSlot(Appointment app, int indexFrom) {
		
		if(app!=null && app.getAppTime()!=null) {
			
			//return (app.getAppTime().minusHours(WORK_START_TIME).getHour() * 60 )- indexFrom; 
			return indexFrom - UtilClass.getStatusIndexFromTime(app.getAppTime());
		}else {
			log.error("########## : "+ERROR_CODE_INVLD_ARG+"->"+ERROR_CODE_INVLD_ARG_MSG);
			throw new InvalidArgumentException(ERROR_CODE_INVLD_ARG);
			
		}
	
	}

	private static boolean isIndexInitialized(int index) {
		// TODO Auto-generated method stub
		return index != INDEX_DEFAULT;
	}
/*
	*//**
	 * Method returns the time slot which is booked in doctors calender which is conflicting with requested time slot
	 * @param app
	 * @param status
	 * @return
	 *//*
	public static TimeSlot getBookedSlotForConflictingAppointmnetRequest(Appointment app, String[] status) {

		
		int indexFrom = app.getAppTime().minusHours(9).getHour() * 60;
		int indexTo = (int)app.getDuration().toMinutes();
		
		if(!status[indexFrom].equals(STATUS_AVAILABLE)) {
			
			while(indexFrom>=0 && !status[indexFrom].equals(STATUS_AVAILABLE)) {
				--indexFrom;
			}
			indexFrom = indexFrom+1;
			
			while(indexTo<status.length && !status[indexTo].equals(STATUS_AVAILABLE)) {
				++indexTo;
			}
			indexTo = indexTo-1;
			
		}
		
		
		return new TimeSlot(indexFrom, indexTo);
	}
	
*/

	public static boolean isAppointmentTimeValid(Appointment app) {
	
		if(app == null || app.getAppTime() == null) {
			log.error(ERROR_CODE_INVLD_ARG+"->"+ ERROR_CODE_INVLD_ARG_MSG);
			throw new InvalidArgumentException(ERROR_CODE_INVLD_ARG);
		}
		

		if(app.getAppTime().isAfter(LocalTime.of(WORK_START_TIME-1, 59,59)) && app.getAppTime().isBefore(LocalTime.of(WORK_END_TIME, 1))) {
			return true;
		}
		return false;
	}	
}
