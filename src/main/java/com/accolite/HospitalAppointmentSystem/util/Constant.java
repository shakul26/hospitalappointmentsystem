package com.accolite.HospitalAppointmentSystem.util;

public class Constant {

	public static final String ERRRO_PATH="/error";
	
	
	/**
	 * ERROR CODE and message
	 */
	public static final String ERROR_CODE_GENERIC = "HSPTL_APP_000";
	public static final String ERROR_CODE_GENERIC_MSG = "Generic Error, Something went Wrong!!";
	
	public static final String ERROR_CODE_ENTY_NT_FND = "HSPTL_APP_001";
	public static final String ERROR_CODE_ENTY_NT_FND_MSG = "Entity does not exist for given key";
	
	public static final String ERROR_CODE_BAD_REQ="HSPTL_APP_002";
	public static final String ERROR_CODE_BAD_REQ_MSG="Validation Failed";
	
	public static final String ERROR_CODE_INVLD_ARG = "HSPTL_APP_003";
	public static final String ERROR_CODE_INVLD_ARG_MSG = "Invalid Argument passed to method";
	
	public static final String ERROR_CODE_DPLCT_ENTITY = "HSPTL_APP_004";
	public static final String ERROR_CODE_DPLCT_ENTITY_MSG="Object Already exist with given key";
	
	public static final String ERROR_CODE_TRNSCTN_FAIL = "HSPTL_APP_005";
	public static final String ERROR_CODE_TRNSCTN_FAIL_MSG = "Operation Failed due to transaction failure";

	public static final String ERROR_CODE_ENTIY_SAVE_FAIL = "HSPTL_APP_006";	
	public static final String ERROR_CODE_ENTIY_SAVE_FAIL_MSG="Object Creation Failed";

	public static final String ERROR_CODE_APP_CNFLCT = "HSPTL_APP_007";	
	public static final String ERROR_CODE_APP_CNFLCT_MSG="Appointment is Conflictinig";

	public static final String ERROR_CODE_INVLD_INDX = "HSPTL_APP_008";	
	public static final String ERROR_CODE_INVLD_INDX_MSG="Invalid Index Value Assigned";

	
	public static final String ERROR_CODE_INVLD_STATUS = "HSPTL_APP_009";	
	public static final String ERROR_CODE_INVLD_STATUS_MSG="Invalid Status Found";

		
	
	
	
	
	/**
	 * URL CONSTANT
	 */
	public static final String SERVER_URL="http://localhost:8080";
	public static final String API_URL="/appntmnt";
	public static final String URL_DCTR="/doctor";
	public static final String URL_ADD="/add";
	public static final String URL_ALL="/all";
	public static final String URL_APPNT="/app";
	

	/**
	 * Status Constant
	 */
	public static final String STATUS_BOOKED= "BOOKED";
	public static final String STATUS_AVAILABLE= "AVAILABLE";
	public static final String STATUS_NOT_AVAILABLE= "NOT_AVAILABLE";

	public static final String STATUS_REST="REST";
	
	
	
	/**
	 * DEFAULT CONSTANT
	 */
	
	public static final int INDEX_DEFAULT=-1;
	public static final int RANK_DEFAULT=0;
	public static final int WORK_START_TIME = 9;
	public static final int WORK_END_TIME = 17;
	public static final int WORK_REST_TIME_IN_MIN = 10;
	
}
