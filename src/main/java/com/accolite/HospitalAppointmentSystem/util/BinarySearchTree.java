package com.accolite.HospitalAppointmentSystem.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_APP_CNFLCT;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_APP_CNFLCT_MSG;

import java.time.LocalTime;
import java.util.ArrayList;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.exception.AppointmentConfilictException;
import com.accolite.HospitalAppointmentSystem.exception.InvalidArgumentException;

public class BinarySearchTree {

	Logger log = LoggerFactory.getLogger(BinarySearchTree.class);
	 public class Node{
		
		Appointment key;
		Node left, right;
		public Node(Appointment k) {
			this.key = k;
			left = null;
			right = null;
		}
		public Appointment getKey() {
			return key;
		}
		public void setKey(Appointment key) {
			this.key = key;
		}
		public Node getLeft() {
			return left;
		}
		public void setLeft(Node left) {
			this.left = left;
		}
		public Node getRight() {
			return right;
		}
		public void setRight(Node right) {
			this.right = right;
		}
		
		
	}
	
    private Node root;
	
	


	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public BinarySearchTree() {
		
		root = null;
	}
	
	public BinarySearchTree(ArrayList<Appointment> al) {
		
		this();
		for(Appointment ap : al) {
			root = insert(ap);
		}
	}

	public Node insert(Appointment key) {
		
		return insrtReccord(root, key);
	}
	
	Node insrtReccord(Node root, Appointment key) {
		
		if(root == null) {
			root = new Node(key);
			return root;
		}
		
		if(!isConflict(root,key)) {
		
			if(key.getAppTime().isBefore(root.key.getAppTime()))
			{
				root.left = insrtReccord(root.left, key);
			}else if(key.getAppTime().isAfter(root.key.getAppTime())){
				root.right = insrtReccord(root.right, key);
			}
			
			return root;
		}else {
		
			log.error(ERROR_CODE_APP_CNFLCT+"->"+ERROR_CODE_APP_CNFLCT_MSG);
			throw new AppointmentConfilictException(ERROR_CODE_APP_CNFLCT);
		}
		
		
	}
	

	private boolean isConflict(Node root, Appointment key) {
		
	    if(root==null || key==null) {
	    	
	    	log.error(ERROR_CODE_INVLD_ARG+"->"+ERROR_CODE_INVLD_ARG_MSG);
	    	throw new InvalidArgumentException(ERROR_CODE_INVLD_ARG);
	    }
		
	   return  UtilClass.isConflict(root.key, key);
	
	}
}
