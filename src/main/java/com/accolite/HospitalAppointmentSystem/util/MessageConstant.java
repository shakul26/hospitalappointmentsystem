package com.accolite.HospitalAppointmentSystem.util;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_END_TIME;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_START_TIME;
public class MessageConstant {
	
	/**
	 * Doctor
	 */
	
	//class : DoctorDaoImpl
	public static final String MSG_ADD_DOCTOR_ARG_NULL = "Doctor object in method addDoctor of class DoctorDaoImpl is null";
	public static final String MSG_IS_DOCTOR_EXIST_ARG_NULL = "Doctor object in method isDoctorExist of class DoctorDaoImpl is null";
	public static final String MSG_IS_DOCTOR_EXIST_PK_NULL = "Primary key of Doctor object in method isDoctorExist of class DoctorDaoImpl is null";
	public static final String MSG_FIND_BY_ID_INVLD_ARG = "Inavlid Argument Passed to the method findById of class DoctorDaoImpl";
	public static final String MSG_FIND_BY_ID_ENTY_NT_FND = "Null object returned in method findById of class DoctorDaoImpl";
	
	
	
	/**
	 * Generic message
	 */
	

	public static final String MSG_SUCCESS_DOCTOR_CREATED = "Doctor Entity Created";
	public static final String MSG_SUCCESS_DOCTOR_UPDATED = "Doctor Entity Updated";
	public static final String MSG_SUCCESS_APPNTMNT_CREATED = "Doctor Entity Updated";

	
	/**
	 * VALIDATION MESSAGE
	 */
	public static final String MSG_VLDTN_FAIL_WRONG_APP_TIME = "Appoinrtment time has to be between"+ WORK_START_TIME+":00 to "+WORK_END_TIME+":00 PM";

}
