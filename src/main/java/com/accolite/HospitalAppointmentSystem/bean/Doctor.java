package com.accolite.HospitalAppointmentSystem.bean;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "doctor")
@PrimaryKeyJoinColumn(name="persion_id")
public class Doctor extends Person{

	@NotNull
	@Column(name = "dep_name")
	private String departmentName;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "doctor")
	private Set<Appointment> appointments = new HashSet<Appointment>(0);

	public Doctor() {
		super();
	}

	public Doctor(int id, String firstName, String lastName, String phoneNumber, String depName){
		super(id,firstName, lastName, phoneNumber);
		this.departmentName = depName;
	}
	
	public Doctor(@NotNull String departmentName, Set<Appointment> appointments) {
		super();
		this.departmentName = departmentName;
		this.appointments = appointments;
	}

	
	
	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Set<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Set<Appointment> appointments) {
		this.appointments = appointments;
	}
	
	
	

}
