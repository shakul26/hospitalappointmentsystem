package com.accolite.HospitalAppointmentSystem.bean;

import java.time.LocalTime;
import static com.accolite.HospitalAppointmentSystem.util.Constant.RANK_DEFAULT;
public class TimeSlot {

	private LocalTime startTime;
	private LocalTime endTime;
	private int rank;
	
	
	
	public TimeSlot(LocalTime startTime, LocalTime endTime, int rank) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.rank = rank;
	}
	
	public TimeSlot(int indexFrom, int indexTo) {
		this.startTime = LocalTime.of(9+indexFrom/60, indexFrom % 60);
		this.endTime = LocalTime.of(9+indexTo/60, indexTo % 60);
		this.rank = RANK_DEFAULT;		
	}
	
	public TimeSlot(int indexFrom, int indexTo, int rank) {
		this(indexFrom, indexTo);
		this.rank = rank;		
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public LocalTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}
	public LocalTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	
	
	
}
