package com.accolite.HospitalAppointmentSystem.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "petient")
@PrimaryKeyJoinColumn(name ="persion_id")
public class Patient extends Person{


	@Column(name = "symptom")
	private String symptom;

	public String getSymptom() {
		return symptom;
	}

	
	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}
	
	
	
}
