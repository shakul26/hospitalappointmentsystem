package com.accolite.HospitalAppointmentSystem.bean;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "appointment")
public class Appointment {

	@NotNull
	@Id
	@Column(name = "app_id")
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String appId;
	
	//@Temporal(TemporalType.DATE)
	@Column(name = "app_date")
	@NotNull
	private LocalDate appDate;
	
	
	//@NotNull
	//@Temporal(TemporalType.TIME)
	@Column(name = "app_time")
	private LocalTime appTime;
	
	//@NotNull
	@Column(name = "duration")
	private Duration duration;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id")
	private Doctor doctor;

	
	@Column(name = "location")
	private String location;

	
	public String getAppId() {
		return appId;
	}


	public void setAppId(String appId) {
		this.appId = appId;
	}





	public LocalDate getAppDate() {
		return appDate;
	}


	

	public LocalTime getAppTime() {
		return appTime;
	}


	public void setAppTime(LocalTime appTime) {
		this.appTime = appTime;
	}


	public Duration getDuration() {
		return duration;
	}


	public void setDuration(Duration duration) {
		this.duration = duration;
	}


	public void setAppDate(LocalDate appDate) {
		this.appDate = appDate;
	}


	public Doctor getDoctor() {
		return doctor;
	}


	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}



	public Appointment(@NotNull String appId, @NotNull LocalDate appDate, @NotNull LocalTime appTime,
			@NotNull Duration duration,  Doctor doctor, String location) {
		super();
		this.appId = appId;
		this.appDate = appDate;
		this.appTime = appTime;
		this.duration = duration;
		this.doctor = doctor;
		this.location = location;
	}


	public Appointment() {
		super();
		}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appDate == null) ? 0 : appDate.hashCode());
		result = prime * result + ((appId == null) ? 0 : appId.hashCode());
		result = prime * result + ((appTime == null) ? 0 : appTime.hashCode());
		result = prime * result + ((doctor == null) ? 0 : doctor.hashCode());
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (appDate == null) {
			if (other.appDate != null)
				return false;
		} else if (!appDate.equals(other.appDate))
			return false;
		if (appId == null) {
			if (other.appId != null)
				return false;
		} else if (!appId.equals(other.appId))
			return false;
		if (appTime == null) {
			if (other.appTime != null)
				return false;
		} else if (!appTime.equals(other.appTime))
			return false;
		if (doctor == null) {
			if (other.doctor != null)
				return false;
		} else if (!doctor.equals(other.doctor))
			return false;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}
	
	
	
	
	
}
