package com.accolite.HospitalAppointmentSystem.service;

import java.util.List;

import com.accolite.HospitalAppointmentSystem.bean.Doctor;


public interface DoctorService {

	public boolean addDoctor(Doctor obj);

	public boolean isDocotorExist(Doctor doc);

	public Doctor findById(int id);

	public boolean updateDoctor(Doctor currentDoctor);

	public List<Doctor> findAllDoctors();



}
