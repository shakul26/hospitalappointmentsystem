package com.accolite.HospitalAppointmentSystem.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.bean.TimeSlot;
import com.accolite.HospitalAppointmentSystem.dao.AppointmentDao;
import com.accolite.HospitalAppointmentSystem.exception.AppointmentConfilictException;
import com.accolite.HospitalAppointmentSystem.util.UtilClass;

@Service
public class AppointmentServiceImpl implements AppointmentService{


	Logger log = LoggerFactory.getLogger(AppointmentServiceImpl.class);

	@Autowired
	private AppointmentDao appointmentDao;
	
	
	public boolean addAppointment(Appointment obj, int doctorId) {
		boolean retVal = false;
		
		log.debug("########## : AppointmentServiceImpl : addAppointment : Start");
		
		
			
			
			appointmentDao.addAppointment(obj,doctorId);
			retVal = true;
		
		
		log.debug("########## : AppointmentServiceImpl : addAppointment : End");
		return retVal;
	}

	
	

	@Override
	public boolean isNewAppointmentConflicting(Appointment app, String[] status) {
		log.debug("########## : AppointmentServiceImpl : isNewAppointmentConflicting : Start");
		boolean retVal = false;
		
	
		retVal = UtilClass.isConflict(app, status);
		
		log.debug("########## : AppointmentServiceImpl : isNewAppointmentConflicting : End : retVal : "+ retVal);
		return retVal;
	}


	@Override
	public ArrayList<TimeSlot> buildSuggestion(Appointment app, String[] status) {
		return UtilClass.builAvailableTimeSlots(app,status);
	
	}


	@Override
	public ArrayList<Appointment> findAllAppointmentForDoctorId(Integer doctorId) {
		
		return appointmentDao.findAllAppointmentForDoctorId(doctorId);
	}
	

}
