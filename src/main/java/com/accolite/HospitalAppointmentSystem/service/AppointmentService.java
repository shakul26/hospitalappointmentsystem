package com.accolite.HospitalAppointmentSystem.service;

import java.util.ArrayList;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.bean.TimeSlot;

public interface AppointmentService {

	public boolean isNewAppointmentConflicting(Appointment app, String[] status);

	public boolean addAppointment(Appointment app, int doctorId);

	public ArrayList<TimeSlot> buildSuggestion(Appointment app, String[] status);

	public ArrayList<Appointment> findAllAppointmentForDoctorId(Integer doctorId);
}
