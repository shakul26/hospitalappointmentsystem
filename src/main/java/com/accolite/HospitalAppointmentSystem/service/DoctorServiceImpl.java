package com.accolite.HospitalAppointmentSystem.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accolite.HospitalAppointmentSystem.bean.Doctor;
import com.accolite.HospitalAppointmentSystem.dao.DoctorDao;

@Service
public class DoctorServiceImpl implements DoctorService{

	Logger log = LoggerFactory.getLogger(DoctorServiceImpl.class);
	@Autowired
	private DoctorDao doctorDao;
	public boolean addDoctor(Doctor obj) {
		boolean retVal = false;
		
		log.debug("########## : DoctorServiceImpl : addDoctor : Start");
		
		try {

			doctorDao.addDoctor(obj);
			retVal = true;
		} catch (Exception e) {
			log.debug(e.getMessage());
			e.printStackTrace();
		}
		
		log.debug("########## : DoctorServiceImpl : addDoctor : End");
		return retVal;
	}
	
	@Override
	public boolean isDocotorExist(Doctor doc) {
		log.debug("########## : DoctorServiceImpl : isDocotorExist : Start");
		
		return doctorDao.isDoctorExist(doc);
				
	}

	@Override
	public Doctor findById(int id) {
		log.debug("########## : DoctorServiceImpl : findById : Start");

		Doctor doc = doctorDao.findById(id);
		log.debug("########## : DoctorServiceImpl : findById : End");
		return doc;
	}

	@Override
	public boolean updateDoctor(Doctor doc) {
		log.debug("########## : DoctorServiceImpl : updateDoctor : Start");
		boolean retVal = false;

		try {

			doctorDao.updateDoctor(doc);
			retVal = true;
		} catch (Exception e) {
			log.debug(e.getMessage());
			e.printStackTrace();
		}
		
		log.debug("########## : DoctorServiceImpl : updateDoctor : End");
		return retVal;
	}

	@Override
	public List<Doctor> findAllDoctors() {
		log.debug("########## : DoctorServiceImpl : findAllDoctors : Start");
		
		try {

			return doctorDao.findAllDoctors();
			
		} catch (Exception e) {
			log.debug(e.getMessage());
			e.printStackTrace();
		}
				
		log.debug("########## : DoctorServiceImpl : findAllDoctors : End");
		return null;
	}

	

}
