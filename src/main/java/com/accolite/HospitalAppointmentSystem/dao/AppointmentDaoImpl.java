package com.accolite.HospitalAppointmentSystem.dao;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG_MSG;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.bean.Doctor;
import com.accolite.HospitalAppointmentSystem.exception.AppointmentConfilictException;
import com.accolite.HospitalAppointmentSystem.exception.InvalidArgumentException;
@Repository
public class AppointmentDaoImpl implements AppointmentDao{
	Logger log = LoggerFactory.getLogger(AppointmentDaoImpl.class);
	
    @PersistenceContext
	private EntityManager em;
    
    @Transactional
	public void addAppointment(Appointment obj, int doctorId) {
		



		log.debug("########## : AppointmentDaoImpl : addAppointment : Start");
		
		boolean flag = true;
		if(obj == null)
			{ 
			  flag = false;
			  log.error(ERROR_CODE_INVLD_ARG+"->"+ERROR_CODE_INVLD_ARG_MSG);
			  throw new InvalidArgumentException(ERROR_CODE_INVLD_ARG);
			}
		
		ArrayList<Appointment> al = findAllAppointmentForDoctorId(doctorId);
		
		for(Appointment o : al) {
			
			if(o.getAppDate().equals(o.getAppDate()) && o.getAppTime().equals(obj.getAppTime())) {
				flag = false;
				throw new AppointmentConfilictException("New Appointment "+ obj.getAppId()+" is conflicting");
				
			}
		}
		
		if(flag) {
		Doctor doc = em.find(Doctor.class, doctorId);
		obj.setDoctor(doc);
		em.merge(obj);
		}
		log.debug("########## : AppointmentDaoImpl : addAppointment : End");
		
	
	}

	@Override
	public ArrayList<Appointment> findAllAppointmentForDoctorId(Integer doctorId) {
		
		ArrayList<Appointment> al = new ArrayList<>();
		
		String hqlQuery = "select a "
						 +"from Appointment a "
						 +"inner join fetch a.doctor d "
						 +"where d.id = :doctorId";
		al = (ArrayList<Appointment>) em.createQuery(hqlQuery, Appointment.class).setParameter("doctorId", doctorId).getResultList();
		
		// remove the line later
		
		Set<Appointment> st = new HashSet<>();
		for(Appointment o : al) {
			st.add(o);
		}
		al = new ArrayList<>(st);
		log.debug("########## : AppointmentDaoImpl : addAppointment : End : size of al "+ al.size());
		return al;
	}

	
	
}
