package com.accolite.HospitalAppointmentSystem.dao;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTY_NT_FND;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG_MSG;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_ADD_DOCTOR_ARG_NULL;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_FIND_BY_ID_ENTY_NT_FND;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_FIND_BY_ID_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_IS_DOCTOR_EXIST_ARG_NULL;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accolite.HospitalAppointmentSystem.bean.Doctor;
import com.accolite.HospitalAppointmentSystem.exception.InvalidArgumentException;

@Repository
public class DoctorDaoImpl implements DoctorDao{


	Logger log = LoggerFactory.getLogger(DoctorDaoImpl.class);
	
    @PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void addDoctor(Doctor objDoctor) {


		log.debug("########## : DoctorDaoImpl : addDoctor : Start");
		
		if(objDoctor == null)
			{ 
			  log.error(ERROR_CODE_INVLD_ARG+"->"+ERROR_CODE_INVLD_ARG_MSG);
			  throw new InvalidArgumentException(ERROR_CODE_INVLD_ARG);
			}
		
	//	startTansaction(objDoctor);
		
		em.merge(objDoctor);
		log.debug("########## : DoctorDaoImpl : addDoctor : End");
		
	}

	/*private void startTansaction(Doctor objDoctor) {
		
		EntityTransaction tx = null;
		
		try {
			
			tx = em.getTransaction();
			tx.begin();
			em.persist(objDoctor);
			tx.commit();
		} catch (Exception e) {
			log.error(ERROR_CODE_TRNSCTN_FAIL,e.getMessage());
			e.printStackTrace();
			tx.rollback();
		}
	}*/

	public boolean isDoctorExist(Doctor objDoctor) {
		log.debug("########## : DoctorDaoImpl : isDoctorExist : Start");
		
		if(objDoctor == null)
		{ 
		  log.error(ERROR_CODE_INVLD_ARG+"->"+MSG_IS_DOCTOR_EXIST_ARG_NULL);
		  throw new InvalidArgumentException(MSG_IS_DOCTOR_EXIST_ARG_NULL);
		}
		
		int key =  objDoctor.getId();
		
		
		
		return em.find(objDoctor.getClass(), key)!=null;
		
	}

	@Override
	public Doctor findById(int id) {
		log.debug("########## : DoctorDaoImpl : findById : Start");
		
		if(id <= 0) {
			log.error(ERROR_CODE_INVLD_ARG+"->"+MSG_FIND_BY_ID_INVLD_ARG);
			throw new InvalidArgumentException(MSG_IS_DOCTOR_EXIST_ARG_NULL);
				
		} 
		
		Doctor doc = em.find(Doctor.class, id); 
		
		if(doc==null) {
			
			log.error(ERROR_CODE_ENTY_NT_FND+"->"+MSG_FIND_BY_ID_ENTY_NT_FND);
			throw  new EntityNotFoundException(ERROR_CODE_ENTY_NT_FND);
		}
		
		
		
		log.debug("########## : DoctorDaoImpl : findById : End");
		return doc;
	}

	@Transactional
	@Override
	public void updateDoctor(Doctor doc) {
		
		log.debug("########## : DoctorDaoImpl : updateDoctor : Start");
		if(doc == null)
		{ 
		  log.error(MSG_ADD_DOCTOR_ARG_NULL);
		  throw new InvalidArgumentException(MSG_ADD_DOCTOR_ARG_NULL);
		}
		
		Session session = em.unwrap(Session.class);
		session.update(doc);
		
		log.debug("########## : DoctorDaoImpl : updateDoctor : End");
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Doctor> findAllDoctors() {
		log.debug("########## : DoctorDaoImpl : findAllDoctors : Start");
			
		String hqlQuery = "from Doctor";
		List<Doctor> doctors = em.createQuery(hqlQuery).getResultList();
		
		if(doctors.isEmpty()) {
			
			log.error(ERROR_CODE_ENTY_NT_FND+"->"+MSG_FIND_BY_ID_ENTY_NT_FND);
			throw  new EntityNotFoundException(ERROR_CODE_ENTY_NT_FND);
		}
		log.debug("########## : DoctorDaoImpl : findAllDoctors : End");
		
		return doctors;
	}

	

	

}
