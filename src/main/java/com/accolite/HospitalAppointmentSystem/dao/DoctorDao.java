package com.accolite.HospitalAppointmentSystem.dao;

import java.util.List;

import com.accolite.HospitalAppointmentSystem.bean.Doctor;

public interface DoctorDao{

	public void addDoctor(Doctor objDoctor);
	public boolean isDoctorExist(Doctor objDoctor);
	public Doctor findById(int id);
	public void updateDoctor(Doctor doc);
	public List<Doctor> findAllDoctors();


}
