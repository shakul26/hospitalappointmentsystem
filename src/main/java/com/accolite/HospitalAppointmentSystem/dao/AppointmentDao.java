package com.accolite.HospitalAppointmentSystem.dao;

import java.util.ArrayList;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;

public interface AppointmentDao {

	void addAppointment(Appointment obj, int doctorId);

	public ArrayList<Appointment> findAllAppointmentForDoctorId(Integer doctorId);

}
