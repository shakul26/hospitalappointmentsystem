package com.accolite.HospitalAppointmentSystem.mock;

import java.time.LocalDate;
import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class AttendenceServiceImpl implements AttendenceService {

	private HashMap<LocalDate, Boolean> leaves = new HashMap<>();
	
	@Override
	public boolean isDoctorOnleave(int doctorId, LocalDate date) {
		
		initLeaves();
		
		
		return leaves.get(date) != null ?  true : false;
	}

	private void initLeaves() {
		leaves.put(LocalDate.of(2018, 9, 12), true);
		leaves.put(LocalDate.of(2018, 9, 14), true);
		leaves.put(LocalDate.of(2018, 9, 16), true);
		leaves.put(LocalDate.of(2018, 9, 18), true);
		
	}

}
