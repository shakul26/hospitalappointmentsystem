package com.accolite.HospitalAppointmentSystem.mock;

import java.time.LocalDate;
import java.util.ArrayList;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;

public interface OTHours {

	public ArrayList<Appointment> getOTHous(int doctorId, LocalDate localDate);



}
