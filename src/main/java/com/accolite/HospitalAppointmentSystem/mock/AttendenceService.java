package com.accolite.HospitalAppointmentSystem.mock;

import java.time.LocalDate;

public interface AttendenceService {


	public boolean isDoctorOnleave(int doctorId, LocalDate date);
}
