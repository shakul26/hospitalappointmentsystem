package com.accolite.HospitalAppointmentSystem.mock;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.bean.Doctor;

@Service
public class OTHoursServiceImpl implements OTHours{

	@Override
	public ArrayList<Appointment> getOTHous(int doctorId, LocalDate date) {
		
		ArrayList<Appointment> al = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

		Doctor doc = new Doctor();
		doc.setId(doctorId);
		al.add(new Appointment("A100", LocalDate.parse("11/09/2018", formatter), LocalTime.of(11, 00, 0), Duration.ofHours(1),  doc, "Delhi"));	
		al.add(new Appointment("A101", LocalDate.parse("11/09/2018", formatter), LocalTime.of(15, 0, 0), Duration.ofHours(1),  doc, "Delhi"));	
		
		return al;
	}

	


}
