package com.accolite.HospitalAppointmentSystem.controller;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTIY_SAVE_FAIL;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTIY_SAVE_FAIL_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_AVAILABLE;
import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_BOOKED;
import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_NOT_AVAILABLE;

import static com.accolite.HospitalAppointmentSystem.util.Constant.STATUS_REST;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_END_TIME;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_START_TIME;
import static com.accolite.HospitalAppointmentSystem.util.Constant.WORK_REST_TIME_IN_MIN;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_SUCCESS_APPNTMNT_CREATED;
import static com.accolite.HospitalAppointmentSystem.util.Constant.URL_APPNT;
import static com.accolite.HospitalAppointmentSystem.util.Constant.URL_ADD;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accolite.HospitalAppointmentSystem.bean.Appointment;
import com.accolite.HospitalAppointmentSystem.bean.TimeSlot;
import com.accolite.HospitalAppointmentSystem.exception.AppointmentConfilictException;
import com.accolite.HospitalAppointmentSystem.exception.ExceptionResponse;
import com.accolite.HospitalAppointmentSystem.exception.InvalidArgumentException;
import com.accolite.HospitalAppointmentSystem.mock.AttendenceService;
import com.accolite.HospitalAppointmentSystem.mock.OTHours;
import com.accolite.HospitalAppointmentSystem.service.AppointmentService;
import com.accolite.HospitalAppointmentSystem.util.UtilClass;


@RestController
public class AppointmentController extends AbstractController{

	Logger log = LoggerFactory.getLogger(AppointmentController.class);
	@Autowired
	private OTHours otService;
	
	@Autowired
	private AppointmentService appService;
	
	@Autowired
	private AttendenceService attndenceService;
	
	//private static HashMap<Integer, String[]> calender = new HashMap<Integer,String[]>();
	
	private static HashMap<Integer, ConcurrentHashMap<LocalDate, String[]> > calender = new HashMap<Integer,ConcurrentHashMap<LocalDate, String[]>>();
	
	
	/**
	 * Creates new appointment and provides suggestion if there is anyconflict
	 * @param app
	 * @param doctorId
	 * @return
	 * @throws InterruptedException 
	 */
	@PostMapping(value=URL_APPNT+URL_ADD+"/{id}")
	public ResponseEntity<?> createAppointment(@RequestBody Appointment app, @PathVariable("id") Integer doctorId) throws InterruptedException{
		ResponseEntity<?> objResponse;
		log.debug("########## : AppointmentController : createAppointment : Start"+app.getAppDate()+ " TIME "+ app.getAppTime());
		/**
		 * Check if appointment is valid that is in working hours
		 */
		if(UtilClass.isAppointmentTimeValid(app)) {
		
			
		/**
		 * Get Appointment details of Doctor from third system
		 */
		ArrayList<Appointment> al = new ArrayList<>();
		Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				al.addAll(otService.getOTHous(doctorId, LocalDate.now()));
				
			}
		});
		final ArrayList<Appointment> bookedAppointments = new ArrayList<>();
		//ArrayList<Appointment> bookedAppointments = appService.findAllAppointmentForDoctorId(doctorId);
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				bookedAppointments.addAll(appService.findAllAppointmentForDoctorId(doctorId));
						
			}
		});
	
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		
		
		al.addAll(bookedAppointments);

		/*for(Appointment o : al)
			{ 
			  System.out.println("APPOINTMNET :: start  "+o.getAppDate()+" :: "+ o.getAppTime());
			 
			  System.out.println("APPOINTMNET :: duration   "+ o.getDuration());
				
			}*/
		
		/**
		 * Initialize the calender
		 */
		intitCalender(al,doctorId);
			
		String status[] = calender.get(doctorId).get(app.getAppDate());
			
		// just for seeing the staus in log remove it
		/*synchronized (log) {
			
		
		 log.debug("########## : doctorId : "+doctorId+" appDate : "+app.getAppDate()+" appTime : "+app.getAppTime()+" status : "+Arrays.toString(status));
			
		}
        */
        if(!appService.isNewAppointmentConflicting(app,status)) {
			//no conflict
			
			synchronized (doctorId) {
				
			
				try {
				
			if(appService.addAppointment(app,doctorId)) {
			 
				
				status = UtilClass.bookMinutes(app,status);
				ConcurrentHashMap<LocalDate, String[]> temp = calender.get(doctorId);
				temp.put(app.getAppDate(),status);
			    calender.put(doctorId, temp);
			    log.debug("########## : doctorId : "+doctorId+" appDate : "+app.getAppDate()+" appTime : "+app.getAppTime()+" status : "+Arrays.toString(status));
			    return new ResponseEntity<String>(MSG_SUCCESS_APPNTMNT_CREATED,HttpStatus.OK);
			    
			}else {
				
				/*
				 * Database save operation for appointment failed
				 */
				log.error("########## : "+ERROR_CODE_ENTIY_SAVE_FAIL+"->"+ERROR_CODE_ENTIY_SAVE_FAIL_MSG);
				return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_ENTIY_SAVE_FAIL, ERROR_CODE_ENTIY_SAVE_FAIL_MSG, new Date()),HttpStatus.CONFLICT);
				
			}
				}catch (AppointmentConfilictException e) {
					// TODO: handle exception
					
					ArrayList<TimeSlot> alSuggestions = appService.buildSuggestion(app,calender.get(doctorId).get(app.getAppDate()));
					return new ResponseEntity<ArrayList<TimeSlot>>(alSuggestions,HttpStatus.OK);
				}
			}
		}else {
			// Appointment conflicting, giving suggestions : Free slots with rank of given slot
			ArrayList<TimeSlot> alSuggestions = appService.buildSuggestion(app,calender.get(doctorId).get(app.getAppDate()));
			return new ResponseEntity<ArrayList<TimeSlot>>(alSuggestions,HttpStatus.OK);
		}
		
	
		
		}else {
			/**
			 * Appointment time invalid
			 */
			log.error("########## : "+ERROR_CODE_INVLD_ARG+"->"+ERROR_CODE_INVLD_ARG_MSG);
			return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_INVLD_ARG, ERROR_CODE_INVLD_ARG_MSG, new Date()),HttpStatus.BAD_REQUEST);
			
		}
	}

	
	

	private void intitCalender(ArrayList<Appointment> al, Integer doctorId) {
		
		log.debug("########## : AppointmentCOntroller : intitCalender start");
		ConcurrentHashMap<LocalDate, String[]> dayBooking = null;
		String status[] = null ;
		
			
			for(Appointment obj : al)
			{   
				
			 // atleast one booking for that doctor is already done be it for anyday
				if(calender.containsKey(doctorId)) {
				 
					  dayBooking = calender.get(doctorId);
					  
					  if(dayBooking!=null)
					  {
						  
						  if(!dayBooking.containsKey(obj.getAppDate())) {
							  
							
							  status= new String[(WORK_END_TIME-WORK_START_TIME)*60+1];
						   
								// doctor is not on leave
								if(!attndenceService.isDoctorOnleave(doctorId, obj.getAppDate()))
								{
									
									updateStatus(dayBooking,status,obj, doctorId);
									
								}else {
							    	// doctor is on leave
							    	
							    	for(int i=0; i<status.length; i++) 
										status[i] = STATUS_NOT_AVAILABLE;
								
									
									dayBooking.put(obj.getAppDate(), status);
									calender.put(doctorId, dayBooking);
								
							    	
							    }
						
						   
						   }else {
							  
							   status = dayBooking.get(obj.getAppDate());
							   if(!attndenceService.isDoctorOnleave(doctorId, obj.getAppDate())) {
								   updateStatus(dayBooking,status,obj, doctorId);
							   }else {
								   for(int i=0; i<status.length; i++) 
										status[i] = STATUS_NOT_AVAILABLE;
								
									
									dayBooking.put(obj.getAppDate(), status);
									calender.put(doctorId, dayBooking);
								
							   }
						   }  
						  
					  }else {
						  log.error(ERROR_CODE_INVLD_ARG+"->"+ERROR_CODE_INVLD_ARG_MSG);
						  throw new InvalidArgumentException(ERROR_CODE_INVLD_ARG);
					  }
					  
				
				}else {
					// there is no booking for the doctor
					dayBooking = new ConcurrentHashMap<LocalDate, String[]>();
					status= new String[(WORK_END_TIME-WORK_START_TIME)*60+1];
					
					// doctor is not on leave
					if(!attndenceService.isDoctorOnleave(doctorId, obj.getAppDate()))
					{
						
						updateStatus(dayBooking,status,obj, doctorId);
						
					}else {
				    	// doctor is on leave
				    	
				    	for(int i=0; i<status.length; i++) 
							status[i] = STATUS_NOT_AVAILABLE;
					
						
						dayBooking.put(obj.getAppDate(), status);
						calender.put(doctorId, dayBooking);
					
				    	
				    }
					
					
				}
				
				

	}// end of for loop of app
			
			

		
		
			log.debug("########## : AppointmentCOntroller : intitCalender : End");

		
	}




	private void updateStatus(ConcurrentHashMap<LocalDate, String[]> dayBooking, String[] status, Appointment obj, int doctorId) {
		

		// marking staus as availble for whole day of the app date	
		for(int i=0; i<status.length; i++)
			status[i] = STATUS_AVAILABLE;
		
			
		/**
		 * Update the status based on appointment
		 */
		//LocalTime appTime = obj.getAppTime();
		//int indexFrom = appTime.minusHours(WORK_START_TIME).getHour() * 60;
		int indexFrom  = UtilClass.getStatusIndexFromTime(obj.getAppTime());
		int indexTo = (int)obj.getDuration().toMinutes();
	
		if(indexFrom > 0 && indexTo > 0 && indexFrom < status.length && indexTo < status.length) {
			
			for(int i = indexFrom;i<=indexTo; i++) {
				
				status[i]=STATUS_BOOKED;
				
			}
			
			for(int i=indexTo+1; i<=indexTo+WORK_REST_TIME_IN_MIN;i++) {

				status[i]=STATUS_REST;
				
			}
		
		
			dayBooking.put(obj.getAppDate(), status);
			calender.put(doctorId, dayBooking);
		
		}else {
			throw new IndexOutOfBoundsException();
		}
		
    
		
		
		
	}
}
