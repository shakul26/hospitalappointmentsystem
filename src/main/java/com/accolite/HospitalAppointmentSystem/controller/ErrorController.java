package com.accolite.HospitalAppointmentSystem.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERRRO_PATH;;
@RestController
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController{

	@Override
	public String getErrorPath() {
		return ERRRO_PATH;
	}
	
	@RequestMapping(value = ERRRO_PATH)
	public String error() {
		
		return "error handling";
	}

	
	
}
