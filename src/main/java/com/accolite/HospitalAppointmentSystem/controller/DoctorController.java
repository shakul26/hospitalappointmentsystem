package com.accolite.HospitalAppointmentSystem.controller;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_DPLCT_ENTITY;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_DPLCT_ENTITY_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTIY_SAVE_FAIL;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTIY_SAVE_FAIL_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTY_NT_FND;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTY_NT_FND_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.URL_ADD;
import static com.accolite.HospitalAppointmentSystem.util.Constant.URL_DCTR;
import static com.accolite.HospitalAppointmentSystem.util.Constant.URL_ALL;

import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_FIND_BY_ID_ENTY_NT_FND;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_SUCCESS_DOCTOR_CREATED;
import static com.accolite.HospitalAppointmentSystem.util.MessageConstant.MSG_SUCCESS_DOCTOR_UPDATED;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.accolite.HospitalAppointmentSystem.bean.Doctor;
import com.accolite.HospitalAppointmentSystem.exception.ExceptionResponse;
import com.accolite.HospitalAppointmentSystem.service.DoctorService;


@RestController
public class DoctorController extends AbstractController {

	Logger log = LoggerFactory.getLogger(DoctorController.class);
	
	@Autowired
	private DoctorService doctorService;
	

	/*@RequestMapping(value = URL_DCTR+URL_ADD+"1", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> createDoctor1(){
	
		System.out.println("############################# CONTROLLER ##############################");
		return new ResponseEntity<String>("success",HttpStatus.OK);
	}*/
	
	@RequestMapping(value = URL_DCTR+URL_ALL, method = RequestMethod.GET, produces = { "application/json" })
    public ResponseEntity<List<Doctor>> listAllDoctors() {
        List<Doctor> doctors = doctorService.findAllDoctors();
        if (doctors.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
            
        }
        return new ResponseEntity<List<Doctor>>(doctors, HttpStatus.OK);
    }
	
	@RequestMapping(value = URL_DCTR+URL_ADD, method = RequestMethod.POST )
	public ResponseEntity<?> createDoctor(@Valid @RequestBody Doctor doc){
		log.debug("########## : DoctorController : createDoctor : creating doctor {} ",doc);
		ResponseEntity<?> objResponse;
		if(doctorService.isDocotorExist(doc)) {
			
			log.error("########## : "+ERROR_CODE_DPLCT_ENTITY+"->"+ERROR_CODE_DPLCT_ENTITY_MSG);
			objResponse =  new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_DPLCT_ENTITY, ERROR_CODE_DPLCT_ENTITY_MSG, new Date()),HttpStatus.CONFLICT);
		}
	
		
		if(doctorService.addDoctor(doc)) {
			objResponse = new ResponseEntity<String>(MSG_SUCCESS_DOCTOR_CREATED, HttpStatus.CREATED);	
		}else {
			log.error("########## : "+ERROR_CODE_ENTIY_SAVE_FAIL+"->"+ERROR_CODE_ENTIY_SAVE_FAIL_MSG);
			objResponse = new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_ENTIY_SAVE_FAIL, ERROR_CODE_ENTIY_SAVE_FAIL_MSG, new Date()),HttpStatus.CONFLICT);
			
		}
		
		log.debug("########## : DoctorController : createDoctor : End");
		return objResponse;
	}
	
	@PutMapping(value = URL_DCTR+"/{id}")
	public ResponseEntity<?> updateDoctor(@PathVariable("id") int id, @RequestBody Doctor pDoctor ){
		log.debug("########## : DoctorController : updateDoctor : updating doctor {} ",pDoctor);
		Doctor currentDoctor =  doctorService.findById(id);
		ResponseEntity<?> objResponse;
		if(currentDoctor==null) {
			
			log.error(ERROR_CODE_ENTY_NT_FND+"->"+MSG_FIND_BY_ID_ENTY_NT_FND);
			objResponse = new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_ENTY_NT_FND, ERROR_CODE_ENTY_NT_FND_MSG, new Date(),MSG_FIND_BY_ID_ENTY_NT_FND), HttpStatus.NOT_FOUND);
		
		}
		
		if(!StringUtils.isEmpty(pDoctor.getFirstName()))
		   currentDoctor.setFirstName(pDoctor.getFirstName());
	
		if(!StringUtils.isEmpty(pDoctor.getLastName()))
		 currentDoctor.setLastName(pDoctor.getLastName());
		
		if(!StringUtils.isEmpty(pDoctor.getPhoneNumber()))
	    	currentDoctor.setPhoneNumber(pDoctor.getPhoneNumber());
	
		if(!StringUtils.isEmpty(pDoctor.getDepartmentName()))
			currentDoctor.setDepartmentName(pDoctor.getDepartmentName());
		
		
		
		if(doctorService.updateDoctor(currentDoctor))
		{
			objResponse = new ResponseEntity<String>(MSG_SUCCESS_DOCTOR_UPDATED, HttpStatus.OK);
		}else {
			
			log.error("########## : "+ERROR_CODE_ENTIY_SAVE_FAIL+"->"+ERROR_CODE_ENTIY_SAVE_FAIL_MSG);
			objResponse = new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_ENTIY_SAVE_FAIL, ERROR_CODE_ENTIY_SAVE_FAIL_MSG, new Date()),HttpStatus.CONFLICT);
			
		}
		
		return objResponse;
	}


	}
