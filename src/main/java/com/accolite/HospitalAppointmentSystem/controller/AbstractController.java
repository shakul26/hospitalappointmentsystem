package com.accolite.HospitalAppointmentSystem.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.accolite.HospitalAppointmentSystem.util.Constant.API_URL;

@RestController
@RequestMapping(value=API_URL)
public class AbstractController {

}
