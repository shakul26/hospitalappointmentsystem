package com.accolite.HospitalAppointmentSystem.exception;

import java.util.Date;

public class ExceptionResponse {

	private String errorCode;
	private String message;
	private Date timestamp;
	private String details;
	
	
	
	
	
	
	public ExceptionResponse(String errorCode, String message, Date timestamp, String details) {
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.timestamp = timestamp;
		this.details = details;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public ExceptionResponse( String errorCode, String message, Date timestamp) {
		super();
		
		this.errorCode = errorCode;
		this.message = message;
		this.timestamp = timestamp;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
