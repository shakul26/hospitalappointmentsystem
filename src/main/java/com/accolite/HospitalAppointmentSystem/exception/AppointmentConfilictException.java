package com.accolite.HospitalAppointmentSystem.exception;

public class AppointmentConfilictException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AppointmentConfilictException(String message) {
		
		super(message);
	}
	
}
