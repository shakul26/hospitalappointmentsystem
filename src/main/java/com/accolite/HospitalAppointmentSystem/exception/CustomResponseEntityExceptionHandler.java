package com.accolite.HospitalAppointmentSystem.exception;

import java.util.Date;






import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_GENERIC;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_GENERIC_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTY_NT_FND;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_ENTY_NT_FND_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_BAD_REQ;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_BAD_REQ_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_ARG_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_DPLCT_ENTITY;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_DPLCT_ENTITY_MSG;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_APP_CNFLCT;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_APP_CNFLCT_MSG;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_INDX;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_INDX_MSG;

import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_STATUS;
import static com.accolite.HospitalAppointmentSystem.util.Constant.ERROR_CODE_INVLD_STATUS_MSG;


@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{

	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleAllException(Exception ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_GENERIC ,ERROR_CODE_GENERIC_MSG,new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj,  HttpStatus.INTERNAL_SERVER_ERROR);
	
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	public final ResponseEntity<ExceptionResponse> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_ENTY_NT_FND , ERROR_CODE_ENTY_NT_FND_MSG, new Date(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(InvalidArgumentException.class)
	public final ResponseEntity<ExceptionResponse> handleNullARgumentException(InvalidArgumentException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_INVLD_ARG,ERROR_CODE_INVLD_ARG_MSG, new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(InvalidIndexException.class)
	public final ResponseEntity<ExceptionResponse> handleInvalidIndexException(InvalidIndexException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_INVLD_INDX,ERROR_CODE_INVLD_INDX_MSG, new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(InvalidStatusException.class)
	public final ResponseEntity<ExceptionResponse> handleInvalidStatusException(InvalidStatusException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_INVLD_STATUS,ERROR_CODE_INVLD_STATUS_MSG, new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(DuplicateEntityException.class)
	public final ResponseEntity<ExceptionResponse> handleDuplicateEntityException(DuplicateEntityException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_DPLCT_ENTITY, ERROR_CODE_DPLCT_ENTITY_MSG, new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(AppointmentConfilictException.class)
	public final ResponseEntity<ExceptionResponse> handleAppointmentConfilictException(AppointmentConfilictException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_APP_CNFLCT, ERROR_CODE_APP_CNFLCT_MSG, new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_FOUND);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,HttpStatus status,WebRequest request){

		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_BAD_REQ , ERROR_CODE_BAD_REQ_MSG, new Date(),ex.getBindingResult().toString());
		
		return new ResponseEntity(obj, HttpStatus.BAD_REQUEST);
		
	}
}
