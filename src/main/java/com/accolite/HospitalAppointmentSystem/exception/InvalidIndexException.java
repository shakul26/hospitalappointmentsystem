package com.accolite.HospitalAppointmentSystem.exception;

public class InvalidIndexException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidIndexException(String message) {
		super(message);
	}
}
